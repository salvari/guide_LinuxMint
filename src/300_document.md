#zo Documentación

## Vanilla LaTeX

Para instalar la versión más reciente de LaTeX hago la instalación desde [ctan](https://www.ctan.org/)

Una vez instalado usamos _equivs_ para generar un paquete `deb` y que nuestro sistema sepa que tenemos _texlive_ instalado.

~~~~
cd ~
mkdir tmp
cd tmp
wget http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz
tar xzf install-tl-unx.tar.gz
cd install-tl-xxxxxx
~~~~

La parte xxxxxx varía en función del estado de la última versión de
LaTeX disponible.

~~~~
sudo ./install-tl
~~~~

Una vez lanzada la instalación podemos desmarcar las opciones que
instalan la documentación y las fuentes. Eso nos obligará a consultar
la documentación on line pero ahorrará practicamente el 50% del
espacio necesario. En mi caso sin doc ni src ocupa 2,3Gb

~~~~
mkdir -p /opt/texbin
sudo ln -s /usr/local/texlive/2020/bin/x86_64-linux/* /opt/texbin
~~~~

Por último para acabar la instalación añadimos `/opt/texbin` al
_PATH_. Para _bash_ y _zsh_ basta con añadir al fichero `~/.profile`
las siguientes lineas:

~~~~
# adds texlive to my PATH
if [ -d "/opt/texbin" ] ; then
    PATH="$PATH:/opt/texbin"
fi
~~~~

En cuanto a _fish_ (si es que lo usas, claro) tendremos que modificar
(o crear) el fichero `~/.config/fish/config.fish` y añadir la
siguiente linea:

~~~~
set PATH $PATH /opt/texbin
~~~~

### Falsificando paquetes

Ya tenemos el _texlive_ instalado, ahora necesitamos que el gestor de
paquetes sepa que ya lo tenemos instalado.

~~~~
sudo apt install equivs --no-install-recommends
mkdir -p /tmp/tl-equivs && cd /tmp/tl-equivs
equivs-control texlive-local
~~~~

Alternativamente para hacerlo más fácil podemos descargarnos un
fichero `texlive-local`ya preparado, ejecutando:

~~~~
wget http://www.tug.org/texlive/files/debian-equivs-2018-ex.txt
/bin/cp -f debian-equivs-2020-ex.txt texlive-local
~~~~

Editamos la versión (si queremos) y procedemos a generar el paquete
_deb_.

~~~~
equivs-build texlive-local
~~~~

El paquete que hemos generado tiene una dependencia: _freeglut3_, hay
que instalarla previamente.

~~~~
sudo apt install freeglut3
sudo dpkg -i texlive-local_2020-1_all.deb
~~~~

Todo listo, ahora podemos instalar cualquier paquete debian que
dependa de _texlive_ sin problemas de dependencias, aunque no hayamos
instalado el _texlive_ de Debian.

### Fuentes

Para dejar disponibles las fuentes opentype y truetype que vienen con
texlive para el resto de aplicaciones:

~~~~
sudo cp $(kpsewhich -var-value TEXMFSYSVAR)/fonts/conf/texlive-fontconfig.conf /etc/fonts/conf.d/09-texlive.conf
sudo nano /etc/fonts/conf.d/09-texlive.conf
~~~~

Borramos la linea:

~~~~
<dir>/usr/local/texlive/20xx/texmf-dist/fonts/type1</dir>
~~~~

Y ejecutamos:

~~~~
sudo fc-cache -fsv
~~~~

Actualizaciones
Para actualizar nuestro _latex_ a la última versión de todos los paquetes:

~~~~
sudo /opt/texbin/tlmgr update --self
sudo /opt/texbin/tlmgr update --all
~~~~

También podemos lanzar el instalador gráfico con:

~~~~
sudo /opt/texbin/tlmgr --gui
~~~~

Para usar el instalador gráfico hay que instalar previamente:

~~~~
sudo apt-get install perl-tk --no-install-recommends
~~~~

Lanzador para el actualizador de _texlive_:

~~~~
mkdir -p ~/.local/share/applications
/bin/rm ~/.local/share/applications/tlmgr.desktop
cat > ~/.local/share/applications/tlmgr.desktop << EOF
[Desktop Entry]
Version=1.0
Name=TeX Live Manager
Comment=Manage TeX Live packages
GenericName=Package Manager
Exec=gksu -d -S -D "TeX Live Manager" '/opt/texbin/tlmgr -gui'
Terminal=false
Type=Application
Icon=system-software-update
EOF
~~~~

## Tipos de letra

Creamos el directorio de usuario para tipos de letra:

~~~~
mkdir ~/.local/share/fonts
~~~~

## Fuentes Adicionales

Me he descargado de internet la fuente [Mensch](https://robey.lag.net/downloads/mensch.ttf)
el directorio de usuario para los tipos de letra: `~/.local/share/fonts`

Además he clonado el repo [_Programming
Fonts_](https://github.com/ProgrammingFonts/ProgrammingFonts) aunque
parece que las fuentes están un poco anticuadas.

~~~~
cd ~/wherever
git clone https://github.com/ProgrammingFonts/ProgrammingFonts
cd ~/.local/share/fonts
ln -s ~/wherever/ProgrammingFonts/Menlo .
~~~~

La fuente Hack la he instalado directamente desde el [sitio
web](https://sourcefoundry.org/hack/)

## Pandoc

_Pandoc_ es un traductor entre formatos de documento. Está escrito en
Python y es increiblemente útil. De hecho este documento está escrito
con _Pandoc_.

Instalado el _Pandoc_ descargando paquete `.deb` desde [la página web
del proyecto](http://pandoc.org/installing.html).

Además descargamos plantillas de Pandoc desde [este
repo](https://github.com/jgm/pandoc-templates) ejecutando los
siguientes comandos:

~~~~
mkdir ~/.pandoc
cd ~/.pandoc
git clone https://github.com/jgm/pandoc-templates templates
~~~~

Las plantillas no son imprescindibles pero si quieres aprender a
usarlas o hacer alguna modificación viene bien tenerlas.

## Algunos editores adicionales

Dos editores para hacer pruebas:

Obsidian

:    Instalado con _appimage_ descargado desde la [página
     web](https://obsidian.md/)

Zettlr

:    Instalado con fichero `.deb` descargado desde [su página
     web](https://www.zettlr.com/)

## Calibre

La mejor utilidad para gestionar tu colección de libros electrónicos.

Ejecutamos lo que manda la página web:

~~~~
sudo -v && wget -nv -O- https://download.calibre-ebook.com/linux-installer.sh | sudo sh /dev/stdin
~~~~

El programa queda instalado en `/opt/calibre`. Se puede desinstalar
con el comando `sudo calibre-unistall`.


Para usar el calibre con el Kobo Glo:

* Desactivamos todos los plugin de Kobo menos el Kobo Touch Extended
* Creamos una columna MyShelves con identificativo #myshelves
* En las opciones del plugin:
    * En la opción Collection columns añadimos las columnas series,#myshelves
    * Marcamos las opciones Create collections y Delete empy collections
    * Marcamos _Modify CSS_
    * Update metadata on device y Set series information

Algunos enlaces útiles:

* (https://github.com/jgoguen/calibre-kobo-driver)
* (http://www.lectoreselectronicos.com/foro/showthread.php?15116-Manual-de-instalaci%C3%B3n-y-uso-del-plugin-Kobo-Touch-Extended-para-Calibre)
* (http://www.redelijkheid.com/blog/2013/7/25/kobo-glo-ebook-library-management-with-calibre)
* (https://www.netogram.com/kobo.htm)

## Scribus

Scribus es un programa libre de composición de documentos. con Scribus
puedes elaborar desde los folletos de una exposición hasta una revista
o un poster.

Instalamos desde los depósitos oficiales de Mint.

Se podría instalar desde ppa cuando lo actualicen para incluir Ubunto
20 con los siguientes comandos:

~~~~
sudo add-apt-repository ppa:scribus/ppa
sudo apt update
sudo apt install scribus scribus-ng scribus-template scribus-ng-doc
~~~~

### Cambiados algunos valores por defecto

He cambiado los siguientes valores en las dos versiones, non están
exactamente en el mismo menú pero no son díficiles de encontrar:

* Lenguaje por defecto: __English__
* Tamaño de documento: __A4__
* Unidades por defecto: __milimeters__
* Show Page Grid: __Activado__
* Dimensiones de la rejilla:
    * Mayor: __30 mm__
    * Menor: __6mm__
* En opciones de salida de _pdf_ indicamos que queremos salida a
  impresora y no a pantalla. Y también que no queremos _spot colors_,
  que serían sólo para ciertas impresoras industriales, así que
  activamos la opción _Convert Spot Colors to Process Colors_.

Siempre se puede volver a los valores por defecto sin mucho problema
(hay una opción para ello)

Referencia [aquí](https://www.youtube.com/watch?v=3sEoYZGABQM&list=PL3kOqLpV3a67b13TY3WxYVzErYUOLYekI)

### Solucionados problemas de _hyphenation_

_Scribus_ no hacia correctamente la separación silábica en castellano,
he instalado los paquetes:

* hyphen-es
* hyphen-gl

Y ahora funciona correctamente.

## Foliate: lector de libros electrónicos

Instalado el paquete deb desde [su propio
github](https://github.com/johnfactotum/foliate/releases)

## Zotero: Gestor de referencias bibliográficas

Por cortesía de [Emiliano Heyns](https://github.com/retorquere/zotero-deb) tenemos disponible el paquete de Zotero para Debian y Ubuntu.

```bash
wget -qO- https://apt.retorque.re/file/zotero-apt/install.sh | sudo bash
sudo apt update
sudo apt install zotero
```

A mayores instalamos el *addon* ***Better Bibtex*** descargando la última versión disponible desde [aquí](https://retorque.re/zotero-better-bibtex/installation/) e instalando en Zotero con la opción "Instalar desde fichero".
