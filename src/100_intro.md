---
title: Bitácora Linux Mint Ulyana 20 / Ulyssa 20.1 / Una 20.3
author:
- Sergio Alvariño <salvari@gmail.com>
tags: [LinuxMint, Pandoc, Documentación, makefile, git]
date: junio-2020
lang: es-ES
subject: Linux Mint 20.4 Ulyana
keywords: linux, postinstalación, mint, ulyana
abstract: |
  Bitácora de mi portatil

  Solo para referencia rápida y personal.
...

# Introducción

Mis portatiles son:

* Un ordenador Acer 5755G con las siguientes
características:

    * Core i5 2430M 2.4GHz

    * NVIDIA Geforce GT 540M (+ intel integrada)

    * 8Gb RAM

    * 750Gb HD

    Este portátil equipa una tarjeta _Nvidia Geforce GT540M_ que
    resulta pertenecer a una rama muerta en el árbol de desarrollo de
    Nvidia.

    Esta tarjeta provocaba todo tipo de problemas de
    sobrecalientamiento, pero en las últimas versiones de Linux
    instalando el driver de Nvidia parece funcionar correctamente.

* Un Lenovo Legion

    * Core i7-9750H

    * Nvidia GTX1650-4Gb (+ intel integrada)

    * 16Gb RAM

    * 512Gb SSD + 1Tb HDD
